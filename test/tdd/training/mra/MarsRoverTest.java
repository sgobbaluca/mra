package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testPlanetContainsObstacle() throws MarsRoverException {
		List<String> planetObstacle = new ArrayList();
		planetObstacle.add("(4,7)");
		planetObstacle.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		
		assertTrue(rover.planetContainsObstacleAt(4,7));
	}
	
	@Test
	public void testRoverExecuteNoCommands() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = ""; 
		
		planetObstacle.add("(4,7)");
		planetObstacle.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		
		assertEquals(null, rover.executeCommand(commands));
	}
	
	@Test
	public void testRoverExecuteTurningRightCommand() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "r"; 
		
		planetObstacle.add("(4,7)");
		planetObstacle.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		
		assertEquals("(0,0,E)", rover.executeCommand(commands));
	}
	
	@Test
	public void testRoverExecuteTurningLeftCommand() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "l"; 
		
		planetObstacle.add("(4,7)");
		planetObstacle.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		
		assertEquals("(0,0,W)", rover.executeCommand(commands));
	}
	
	@Test
	public void testRoverMovingForward() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "f"; 
		
		planetObstacle.add("(4,7)");
		planetObstacle.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		rover.position= "(7,6,N)";
		
		assertEquals("(7,7,N)", rover.executeCommand(commands));
	}

	@Test
	public void testRoverMovingBackward() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "b"; 
		
		planetObstacle.add("(4,7)");
		planetObstacle.add("(2,3)");
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		rover.position= "(5,8,E)";
		
		assertEquals("(4,8,E)", rover.executeCommand(commands));
	}
	
	@Test
	public void testRoverMovingCombined() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "ffrff"; 
		
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		
		assertEquals("(2,2,E)", rover.executeCommand(commands));
	}
	
	@Test
	public void testRoverMovingEdge() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "b"; 
		
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		
		assertEquals("(0,9,N)", rover.executeCommand(commands));
	}
	
	@Test
	public void testRoverMovingWithObstacle() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "ffrfff"; 
		
		planetObstacle.add("(2,2)");
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		
		assertEquals("(1,2,E)(2,2)", rover.executeCommand(commands));
	}

	@Test
	public void testRoverMovingWithMultipleObstacle() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "ffrfffrflf"; 
		
		planetObstacle.add("(2,2)");
		planetObstacle.add("(2,1)");
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		
		assertEquals("(1,1,E)(2,2)(2,1)", rover.executeCommand(commands));
	}
	
	@Test
	public void testRoverMovingWithObstacleOnEdge() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "b"; 
		
		planetObstacle.add("(0,9)");
		MarsRover rover = new MarsRover(10, 10, planetObstacle);
		
		assertEquals("(0,0,N)(0,9)", rover.executeCommand(commands));
	}
	
	@Test
	public void testRoverMovingAroundPlanet() throws MarsRoverException{
		List<String> planetObstacle = new ArrayList();
		String commands = "ffrfffrbbblllfrfrbbl";
		
		planetObstacle.add("(2,2)");
		planetObstacle.add("(0,5)");
		planetObstacle.add("(5,0)");
		MarsRover rover = new MarsRover(6, 6, planetObstacle);
		
		assertEquals("(0,0,N)(2,2)(0,5)(5,0)", rover.executeCommand(commands));
	}
		
}
