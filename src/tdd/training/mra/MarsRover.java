package tdd.training.mra;

import java.util.List;

public class MarsRover {
	private int[][] planet;
	public String position;
	private int X;
	private int Y;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		this.planet = new int[planetX][planetY];
		this.X = planetX;
		this.Y = planetY;
		position = "(0,0,N)";
		
		for(int i=0; i<planetObstacles.size(); i++) {
			int tmpX = Integer.parseInt(planetObstacles.get(i).substring(1, planetObstacles.get(i).indexOf(",")));
			int tmpY = Integer.parseInt(planetObstacles.get(i).substring(planetObstacles.get(i).indexOf(",")+1, planetObstacles.get(i).length()-1));
			
			planet[tmpX][tmpY] = 1;
		}
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return planet[x][y] == 1;
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String obstacles = "";
		String tmpObstacle;
		int tmpX, tmpY;
		
		if(commandString.length() != 0) {
			for(int i=0; i<commandString.length(); i++) {
				tmpObstacle = "";
				
				switch(commandString.charAt(i)) {
					case 'r':
						rotateRover('r');
						break;
					case'l':
						rotateRover('l');
						break;
					case 'f':
						move(1);
						
						tmpX = Integer.parseInt(position.substring(position.indexOf("(")+1, position.indexOf(",")));
						tmpY = Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3));
						
						if(planetContainsObstacleAt(tmpX, tmpY)){
							tmpObstacle = "(" + String.valueOf(tmpX) + "," + String.valueOf(tmpY) + ")";
							if(!obstacles.contains(tmpObstacle)) obstacles += tmpObstacle;
							
							move(-1);
						}
						break;
					case 'b':
						move(-1);
						
						tmpX = Integer.parseInt(position.substring(position.indexOf("(")+1, position.indexOf(",")));
						tmpY = Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3));
						
						if(planetContainsObstacleAt(tmpX, tmpY)){
							tmpObstacle = "(" + String.valueOf(tmpX) + "," + String.valueOf(tmpY) + ")";
							if(!obstacles.contains(tmpObstacle)) obstacles += tmpObstacle;
							
							move(1);
						}
						break;
				}
			}
			position += obstacles;
			return position;
		}
		return null;
	}
	
	public void rotateRover(char rotation) {
		if(rotation == 'r') {
			switch(position.charAt(position.length()-2)) {
				case 'N':
					position = position.substring(0, position.length()-2) + "E)";
					break;
				case 'E':
					position = position.substring(0, position.length()-2) + "S)";
					break;
				case 'S':
					position = position.substring(0, position.length()-2) + "W)";
					break;
				case 'W':
					position = position.substring(0, position.length()-2) + "N)";
					break;
			}
		}else {
			switch(position.charAt(position.length()-2)) {
			case 'N':
				position = position.substring(0, position.length()-2) + "W)";
				break;
			case 'E':
				position = position.substring(0, position.length()-2) + "N)";
				break;
			case 'S':
				position = position.substring(0, position.length()-2) + "E)";
				break;
			case 'W':
				position = position.substring(0, position.length()-2) + "S)";
				break;
			}
		}
	}
	
	public void move(int move) {
		switch(position.charAt(position.length()-2)) {
		case 'N':
			if(Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3)) + move == X) position = position.substring(0, position.indexOf(",")+1) + "0" + position.substring(position.length()-3, position.length());
			else if(Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3)) + move == -1) position = position.substring(0, position.indexOf(",")+1) + String.valueOf(X-1) + position.substring(position.length()-3, position.length());
			else position = position.substring(0, position.indexOf(",")+1) + String.valueOf(Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3)) + move) + position.substring(position.length()-3, position.length());
			
			break;
		case 'E':
			if(Integer.parseInt(position.substring(position.indexOf("(")+1, position.indexOf(","))) + move == X) position = "(" + "0" + position.substring(position.indexOf(","), position.length());
			else if(Integer.parseInt(position.substring(position.indexOf("(")+1, position.indexOf(","))) + move == -1) position = "(" + String.valueOf(X-1) + position.substring(position.indexOf(","), position.length());
			else position = "(" + String.valueOf(Integer.parseInt(position.substring(position.indexOf("(")+1, position.indexOf(","))) + move) + position.substring(position.indexOf(","), position.length());
			break;
		case 'S':
			if(Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3)) - move == X) position = position.substring(0, position.indexOf(",")+1) + "0" + position.substring(position.length()-3, position.length());
			else if(Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3)) - move == -1) position = position.substring(0, position.indexOf(",")+1) + String.valueOf(X-1) + position.substring(position.length()-3, position.length());
			else position = position.substring(0, position.indexOf(",")+1) + String.valueOf(Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3)) - move) + position.substring(position.length()-3, position.length());
			break;
		case 'W':
			if(Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3)) - move == X) position = "(" + "0" + position.substring(position.indexOf(","), position.length());
			else if(Integer.parseInt(position.substring(position.indexOf(",")+1, position.length()-3)) - move == -1) position = "(" + String.valueOf(X-1) + position.substring(position.indexOf(","), position.length());
			else position = "(" + String.valueOf(Integer.parseInt(position.substring(position.indexOf("(")+1, position.indexOf(","))) - move) + position.substring(position.indexOf(","), position.length());
			break;
		}
	}

}
